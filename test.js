/**
 * @description: This file contains all of the uit tests written to test
 * the main portion of the code.
 *
 * @module: test.js
 *
 * @author: Austin Bieber
 */

// Node modules
const fs = require('fs');

// NPM Modules
const chai = require('chai');
const formatter = require('./index.js');
const testData = JSON.parse(fs.readFileSync('./test-data.json').toString());

// Define the unit tests
describe('Unit Tests', () => {
	it('should retrieve data from a url', testRetrieveData);
	it('should test that the Stranger Things data is formatted properly', testParseStrangerThingsData);
});

/**
 * @description: Verifies that the data can be retrieved and returned as a JSON object.
 */
async function testRetrieveData() {
	const data = await formatter.retrieveData("http://api.tvmaze.com/singlesearch/shows?q=stranger-things&embed=episodes");

	// Verify data is a JSON object
	chai.expect(typeof data).to.equal('object');
	// Make sure data is no null, since null is an object type
	chai.expect(data).to.not.equal(null);
}

/**
 * @description: Verifies the main function returns data in the expected format.
 */
async function testParseStrangerThingsData() {
	const formattedData = await formatter.parseStrangerThingsData();

	// Expect the one and only key to be the id
	chai.expect(Object.keys(formattedData)[0]).to.equal(testData.id.toString());
	chai.expect(Object.keys(formattedData).length).to.equal(1);

	// Expect that object to only contain the specified keys
	chai.expect(formattedData[testData.id]).to.have.keys(['totalDurationSec', 'averageEpisodesPerSeason',
		'episodes']);

	// Expect those fields to be of the correct type
	chai.expect(Number.isInteger(formattedData[testData.id].totalDurationSec)).to.equal(true);
	chai.expect(typeof formattedData[testData.id].averageEpisodesPerSeason).to.equal('number');
	chai.expect(Number.isInteger(formattedData[testData.id].averageEpisodesPerSeason)).to.equal(false);
	chai.expect(typeof formattedData[testData.id].episodes).to.equal('object');

	// Convert testData episode to an object for easier lookup in tests
	const testEpisodes = {}
	testData._embedded.episodes.forEach((e) => {
		testEpisodes[e.id] = e;
	});

	// Verify the correct number of episodes
	chai.expect(Object.keys(formattedData[testData.id].episodes).length).to.equal(testData._embedded.episodes.length);

	// Verify each episode
	Object.keys(formattedData[testData.id].episodes).forEach((k) => {
		const episode = formattedData[testData.id].episodes[k];
		const testEpisode = testEpisodes[k]

		// Verify data for each episode
		chai.expect(episode).to.have.keys(['sequenceNumber', 'shortTitle', 'airTimestamp', 'shortSummary']);
		chai.expect(episode.sequenceNumber).to.equal(`s${testEpisode.season}e${testEpisode.number}`);
		chai.expect(testEpisode.name).to.include(episode.shortTitle);
		if (testEpisode.summary != null) {
			chai.expect(testEpisode.summary).to.include(episode.shortSummary);
		}
		else {
			chai.expect(episode.shortSummary).to.equal('N/A');
		}
		chai.expect(Number.isInteger(episode.airTimestamp)).to.equal(true);
	});
}