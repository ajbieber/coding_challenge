# Charter Coding Challenge

Contained in this repo is the solution for the Stranger Things Code
Challenge. All of the code exists in two files, `index.js` and `test.js`.
The logic for retrieving the data from the provided website and formatting
it exists in `index.js`, while all of the unit tests exist in `test.js`.

### Installation & Testing
When initially downloading this repo, you will need to run `npm install`
within the directory to install the necessary dependencies. These include
`axios` for making requests and `chai` and `mocha` for the unit tests.
Mocha was selected out of pure simplicity to use, and Chai was added
alongside it to increase assertion capabilites. Axios was selected once
again out of desire for a simplistic module to GET the data.

To run the tests, simply run the command `npm test`. This should output
the results of the two tests.

### Further Work
In total, the development of the code and tests took me around 90 minutes.
To stay within the 2 hour time limit, I cut myself short on some of the tests.
If I had more time, I would have added more tests to test out all of the
helper functions in `index.js`, as well as testing invalid input.