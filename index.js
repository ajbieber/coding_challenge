/**
 * @description: Defines functions for retrieving data from a URL,
 * as well as formatting that data in a specific, more human friendly format.
 *
 * @module: index.js
 *
 * @author: Austin Bieber
 */

// NPM Modules
const axios = require('axios');

/**
 * @description Formats TV show data in the desired format.
 *
 * NOTE: Summary regex taken from https://www.w3resource.com/javascript-exercises/javascript-string-exercise-35.php
 *
 * @param {object} data - The data to format in a JSON object.
 *
 * @returns {object} The formatted data.
 */ 
function formatData(data) {
	const id = data.id;

	// Create the base object to be returned
	const formattedObj = {
		[id]: {
			totalDurationSec: 0,
			averageEpisodesPerSeason: 0.0,
			episodes: {

			}
		}
	};

	totalDuration = 0
	seasonEpisodeCount = []

	// Loop over each episode only once
	data._embedded.episodes.forEach((episode) => {
		// Add episode data to the formattedObject
		formattedObj[id]['episodes'][episode.id] = {
			sequenceNumber: `s${episode.season}e${episode.number}`,
			shortTitle: parseTitle(episode),
			airTimestamp: parseTimeStamp(episode),
			shortSummary: parseSummary(episode)
		}

		// Add episode duration to running count
		totalDuration += episode.runtime;

		// Update season count
		if (!seasonEpisodeCount[episode.season - 1]) {
			seasonEpisodeCount[episode.season - 1] = 1;
		}
		else {
			seasonEpisodeCount[episode.season - 1] += 1;
		}
	});

	// Update the totlaDurationSec and averageEpisodesPerSeason fields
	formattedObj[id].totalDurationSec = totalDuration * 60;
	formattedObj[id].averageEpisodesPerSeason = parseFloat((seasonEpisodeCount.reduce((a,b) => a + b, 0) / seasonEpisodeCount.length).toFixed(1));


	return formattedObj;
}

/**
 * @description: Helper function for parsing an episodes summary
 * into the desired format for the "shortSummary".
 *
 * @param {object} episode - The episode to format.
 *
 * @param {string} The reduced summary.
 */
function parseSummary(episode) {
	if (episode.summary) {
		// Remove HTML, split on period, grab first sentence and re-append period
		return episode.summary.replace(/<[^>]*>/g, '').split('.')[0] + '.';
	}
	else {
		// Handle special case where summary is null (episode not yet released)
		return "N/A";
	}
}

/**
 * @description: Helper function for parsing an episodes airstamp
 * into the desired format for the "airTimestamp".
 *
 * @param {object} episode - The episode to format.
 *
 * @param {number} The formatted timestamp.
 */
function parseTimeStamp(episode) {
	// Convert to date object, grab epoch time in ms, divide by 1000 to get seconds
	return new Date(episode.airstamp).getTime() / 1000;
}

/**
 * @description: Helper function for parsing an episodes name
 * into the desired format for the "shortTitle".
 *
 * @param {object} episode - The episode to format.
 *
 * @param {string} The formatted title.
 */
function parseTitle(episode) {
	// Split off the Chapter XXX: part
	return episode.name.split(/Chapter [a-zA-Z]+: /)[1];
}


/**
 * @description: Retrieves data from a url and returns it as valid JSON.
 *
 * @param {string} url - The url to get the data from.
 *
 * @returns {object} Data in JSON object.
 */
async function retrieveData(url) {
	try {
		const response = await axios.get(url);
		return response.data;
	}
	catch (error) {
		console.log(`Failed to retrieve data from ${url}.`);
		console.log(error);
	}
}

/**
 * @description Main function for the coding challenge. Grabs data about the
 * Stranger Things TV series, formats it to the specified format, and returns it.
 *
 * @returns {object} The formatted Stranger Things data.
 */
async function parseStrangerThingsData() {
	const url = "http://api.tvmaze.com/singlesearch/shows?q=stranger-things&embed=episodes";
	const data = await retrieveData(url);
	return formatData(data);
}

// Exports
module.exports = {
	retrieveData,
	parseStrangerThingsData
}